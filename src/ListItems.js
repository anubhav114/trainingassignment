import React from 'react';
import './ListItems.css';

function ListItems (props){
    const items = props.items;
    const listItems = items.map(item =>
         {
          return <div className ="list" key={item.key}>
          <p>
              <input type = "text" id = {item.key} value = {item.text} />

              <span id = "btn1"><button id="bt1" onClick={()=>{
                 props.setUpdate(prompt("Edit your text",item.text),item.key)  
                 }
                 }
          >Edit</button>
           </span>

               <span id = "btn2">
               <button id = "bt2"  onClick={() => props.deleteItem(item.key)}>Delete</button>
               </span>
           </p>
           
          </div>
    })
    return(
        <div>{listItems}</div>
        
    )
}
export default ListItems;